package gqz.calculator_kotlin

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : Activity(), MainInterface {

    override fun testFun1(a: Int, b: Double) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private var TAG = "MainActivity-->"

    //定义控件,?表示该变量的值可以为空
    var a: Int = 1
    var b: Int? = null
    private var tv: TextView? = null
    private var et: EditText? = null
    private var btn: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
        var list: Array<String>
        var i = 0
        myFun6()
    }

    fun initView() {
        Log.i(TAG, "initView")
        tv = findViewById(R.id.tv) as TextView
        et = findViewById(R.id.et) as EditText
        btn = findViewById(R.id.btn) as Button
        btn!!.setOnClickListener {
            Toast.makeText(this, "don't click me", 0).show()
            tv!!.setText(et!!.text)
            Log.i(TAG, testFun2(1.1f, 1).toString())
        }
    }

    //定义函数
    //无返回值函数,Unit类似Java中的void,可省略:Unit
    fun myFun1(): Unit {
        Log.i(TAG, "no return function!")
    }

    fun myFun2(user: User) {
        Log.i(TAG, "no return function!")
        Log.i(TAG, "name:$user")
    }

    //有返回值函数, 函数关键字 函数名(参数名:参数类型，···):返回值类型{}
    fun myFun3(a: Int, b: Int): Int {
        return a + b;
    }

    //表达式作为函数体，返回类型自动推断
    fun myFun4(a: Int, b: Int) = a + b

    public fun myFun5(a: Int, b: Int): Int = a + b   // public 方法则必须明确写出返回类型

    fun myFun6() {
        var user: User = User("gowcage", "123456")
        user = User("321123")
        Log.i(TAG, "username=${user.username}")
        Log.i(TAG, "password=${user.password}")
        user.id = 11
        Log.i(TAG, "id=${user.id}")
    }

}

//类定义，Koltin 中的类可以有一个 主构造器，以及一个或多个次构造器，主构造器是类头部的一部分，位于类名称之后
//如果主构造器没有任何注解，也没有任何可见度修饰符，那么constructor关键字可以省略
//class User private constructor(name: String, pwd: String) {
class User(name: String, pwd: String) {

    //次构造函数，必须:this(主构造函数的参数，并初始化)
    constructor(pwd: String) : this("gowcage", "") {
        password = pwd
    }

    var username: String? = name
        get() = field?.toUpperCase()
        set

    var password: String? = pwd
    var id: Int = 0
        get() = field
        set(value) {
            if (value > 10) field = -1
            else field = value
        }

    var savePwd: Boolean = false

    //类每次构造都会调动
    init {
        Log.i("User", "this is User class init()")
    }

}

interface MainInterface {
    fun testFun1(a: Int, b: Double)

    fun testFun2(a: Float, b: Int): Float {
        return a + b.toFloat();
    }
}
