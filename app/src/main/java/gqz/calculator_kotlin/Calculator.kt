package gqz.calculator_kotlin

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_calculator.*
import java.math.BigDecimal


/**
 * Created by Administrator on 2017/11/15.
 */

class Calculator : Activity(), View.OnClickListener {

    private val TAG: String = "Calculator-->"

    private var isOperator: Boolean = false

    private var list: ArrayList<MyUnit> = ArrayList()

    private var s: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculator)
//        s = System.currentTimeMillis()
        initView()
//        Log.v(TAG, "use time:${System.currentTimeMillis() - s}")
    }

    private fun initView() {

        for (i in 0..cal_gridLayout.getChildCount() - 1) {
            val button = cal_gridLayout.getChildAt(i) as Button
            button.setWidth(windowManager.defaultDisplay.width / cal_gridLayout.columnCount)
            if (i == cal_gridLayout.getChildCount() - 4)
                button.setHeight((windowManager.defaultDisplay.height - 160) / cal_gridLayout.rowCount * 2)
            else
                button.setHeight((windowManager.defaultDisplay.height - 160) / cal_gridLayout.rowCount)
        }

        cal_tv.text = "0"
        cal_btn_clear.setOnClickListener(this)
        cal_btn_backspace.setOnClickListener(this)
        cal_btn_1.setOnClickListener(this)
        cal_btn_2.setOnClickListener(this)
        cal_btn_3.setOnClickListener(this)
        cal_btn_4.setOnClickListener(this)
        cal_btn_5.setOnClickListener(this)
        cal_btn_6.setOnClickListener(this)
        cal_btn_7.setOnClickListener(this)
        cal_btn_8.setOnClickListener(this)
        cal_btn_9.setOnClickListener(this)
        cal_btn_0.setOnClickListener(this)
        cal_btn_dot.setOnClickListener(this)
        cal_btn_add.setOnClickListener(this)
        cal_btn_sub.setOnClickListener(this)
        cal_btn_mult.setOnClickListener(this)
        cal_btn_division.setOnClickListener(this)
        cal_btn_modulo.setOnClickListener(this)
        cal_btn_equal.setOnClickListener(this)

        var overflow = 0
        cal_tv.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
//                Log.i(TAG, "afterTextChanged:$s")
//                Log.i(TAG, "afterTextChanged:overflow=$overflow,textSize=${cal_tv.textSize}")
                if (overflow > 0 && cal_tv.textSize > 20)
                    cal_tv.textSize -= 5
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
//                Log.i(TAG, "beforeTextChanged:s=$s,start=$start,count=$count,after=$after")
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                overflow = cal_tv.layout.getEllipsisCount(cal_tv.lineCount - 1)
//                Log.i(TAG, "onTextChanged:s=$s,start=$start,count=$count,before=$before")
                if (overflow <= 0 && count < before && cal_tv.textSize < 70)
                    cal_tv.textSize += 2
            }
        })
    }

    //转换表达式
    private fun convertExpression(expression: String) {
        var charArr: CharArray = expression.toCharArray()
        if ("+-*/%".contains(charArr[charArr.size - 1]) || "+-*/%".contains(charArr[0])) {
            Toast.makeText(this, "请输入正确的算术式", Toast.LENGTH_SHORT).show()
            return
        }
//        for(i in charArr)//循环遍历任何提供了迭代器的对象
        var tmp = 0
        for (i in charArr.indices) {//通过索引遍历一个数组或者一个 list
            if (i == tmp) {
                var unit = MyUnit()
                unit.value = charArr.get(i).toString()

                //将数值组合到一起
                for (j in i..charArr.size - 1) {
                    if ("+-*/%".contains(charArr.get(j).toString())) {//判断当前是否运算符
                        tmp = j + 1
                        break
                    }
                    if (j + 1 == charArr.size)
                        break
                    if ("+-*/%".contains(charArr.get(j + 1).toString())) {//判断下一个是否运算符
                        tmp = j + 1
                        break
                    } else {
                        unit.addValue(charArr.get(j + 1).toString())
                    }
                }
                list.add(unit)
            }
        }
        Log.i(TAG, "")
    }

    //计算
    private fun operation(expression: String) {
        if (expression != "")
            convertExpression(expression)
        operation2()
    }
    //计算，先做高级运算
    private fun operation2() {
        var i = 0
        while (i < list.size - 1) {
            var unit: MyUnit = list.get(i)!!
            if (unit.pri == 2) {// * / % 高级运算
                var num1: MyUnit = list.get(i - 1)!!
                var num2: MyUnit = list.get(i + 1)!!
                unit.value = operation3(num1.value, num2.value, unit.value).toString()
                list.removeAt(i - 1)
                list.removeAt(i)
                i--
            }
            i++
        }
        i = 0
        while (i < list.size - 1) {
            var unit: MyUnit = list.get(i)!!
            if (unit.pri == 1) {// + - 低级运算
                var num1: MyUnit = list.get(i - 1)!!
                var num2: MyUnit = list.get(i + 1)!!
                unit.value = operation3(num1.value, num2.value, unit.value).toString()
                list.removeAt(i - 1)
                list.removeAt(i)
                i--
            }
            i++
        }
        Log.i(TAG, "result=${list[0].value}")
        var str: String = list[0].value.replace(".0", "")

        val bd = BigDecimal(str.toDouble())
        str = bd.setScale(2, BigDecimal.ROUND_HALF_UP).toDouble().toString()

        cal_tv.text = str
        list.clear()
    }
    //计算,两个数之间的计算
    private fun operation3(num1: String, num2: String, oper: String): Double {
        var n1: Double = num1.toDouble()
        var n2: Double = num2.toDouble()
        when (oper) {
            "+" -> return n1 + n2
            "-" -> return n1 - n2
            "*" -> return n1 * n2
            "/" -> return n1 / n2
            "%" -> return n1 % n2
        }
        return 0.0
    }

    //检查textview不为空或0
    private fun checkTextView(): Boolean {
        var tmp: String = cal_tv.text.toString()
        if (tmp != "" && tmp != "0")
            return true
        return false
    }

    private fun getText(): String {
        return cal_tv.text.toString()
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.cal_btn_clear -> {
                Log.i(TAG, "clear!")
                cal_tv.text = "0"
                cal_tv.textSize = 70f
                list.clear()
            }
            R.id.cal_btn_backspace -> {
                Log.i(TAG, "backspace!")
                if (checkTextView()) {
                    var tmp: String = getText()
                    if (tmp.substring(0, tmp.length - 1) == "") cal_tv.text = "0"
                    else cal_tv.text = tmp.substring(0, tmp.length - 1)
                    isOperator = true
                }
            }
            R.id.cal_btn_1 -> {
                Log.i(TAG, "1")
                if (checkTextView()) {
                    cal_tv.text = getText() + "1"
                } else {
                    cal_tv.text = "1"
                }
                isOperator = true
            }
            R.id.cal_btn_2 -> {
                Log.i(TAG, "2")
                if (checkTextView()) {
                    cal_tv.text = getText() + "2"
                } else {
                    cal_tv.text = "2"
                }
                isOperator = true
            }
            R.id.cal_btn_3 -> {
                Log.i(TAG, "3")
                if (checkTextView()) {
                    cal_tv.text = getText() + "3"
                } else {
                    cal_tv.text = "3"
                }
                isOperator = true
            }
            R.id.cal_btn_4 -> {
                Log.i(TAG, "4")
                if (checkTextView()) {
                    cal_tv.text = getText() + "4"
                } else {
                    cal_tv.text = "4"
                }
                isOperator = true
            }
            R.id.cal_btn_5 -> {
                Log.i(TAG, "5")
                if (checkTextView()) {
                    cal_tv.text = getText() + "5"
                } else {
                    cal_tv.text = "5"
                }
                isOperator = true
            }
            R.id.cal_btn_6 -> {
                Log.i(TAG, "6")
                if (checkTextView()) {
                    cal_tv.text = getText() + "6"
                } else {
                    cal_tv.text = "6"
                }
                isOperator = true
            }
            R.id.cal_btn_7 -> {
                Log.i(TAG, "7")
                if (checkTextView()) {
                    cal_tv.text = getText() + "7"
                } else {
                    cal_tv.text = "7"
                }
                isOperator = true
            }
            R.id.cal_btn_8 -> {
                Log.i(TAG, "8")
                if (checkTextView()) {
                    cal_tv.text = getText() + "8"
                } else {
                    cal_tv.text = "8"
                }
                isOperator = true
            }
            R.id.cal_btn_9 -> {
                Log.i(TAG, "9")
                if (checkTextView()) {
                    cal_tv.text = getText() + "9"
                } else {
                    cal_tv.text = "9"
                }
                isOperator = true
            }
            R.id.cal_btn_0 -> {
                Log.i(TAG, "0")
                if (checkTextView()) {
                    cal_tv.text = getText() + "0"
                    isOperator = true
                } else {
                    cal_tv.text = "0"
                }
            }
            R.id.cal_btn_dot -> {
                Log.i(TAG, ".")
                if (isOperator) {
                    if (checkTextView()) {
                        cal_tv.text = getText() + "."
                    } else {
                        cal_tv.text = "."
                    }
                    isOperator = false
                }
            }
            R.id.cal_btn_add -> {
                Log.i(TAG, "+")
                if (isOperator) {
                    if (checkTextView()) {
                        cal_tv.text = getText() + "+"
                    } else {
                        cal_tv.text = "+"
                    }
                    isOperator = false
                }
            }
            R.id.cal_btn_sub -> {
                Log.i(TAG, "-")
                if (isOperator) {
                    if (checkTextView()) {
                        cal_tv.text = getText() + "-"
                    } else {
                        cal_tv.text = "-"
                    }
                    isOperator = false
                }
            }
            R.id.cal_btn_mult -> {
                Log.i(TAG, "*")
                if (isOperator) {
                    if (checkTextView()) {
                        cal_tv.text = getText() + "*"
                    } else {
                        cal_tv.text = "*"
                    }
                    isOperator = false
                }
            }
            R.id.cal_btn_division -> {
                Log.i(TAG, "/")
                if (isOperator) {
                    if (checkTextView()) {
                        cal_tv.text = getText() + "/"
                    } else {
                        cal_tv.text = "/"
                    }
                    isOperator = false
                }
            }
            R.id.cal_btn_modulo -> {
                Log.i(TAG, "%")
                if (isOperator) {
                    if (checkTextView()) {
                        cal_tv.text = getText() + "%"
                    } else {
                        cal_tv.text = "%"
                    }
                    isOperator = false
                }
            }
            R.id.cal_btn_equal -> {
                Log.i(TAG, "=")
                s = System.currentTimeMillis()
                operation(cal_tv.text.toString())
                Log.v(TAG, "use time:${System.currentTimeMillis() - s}")
            }
            else -> Log.i(TAG, "nothing!")
        }
    }
}

class MyUnit {
    //    var id: Int = 0//表达式的顺序，递增
    var value: String = ""//数值或运算符
        set(value) {
            field = value
            pri = getPriority()
        }
    var pri: Int = 0//优先级，0最低(普通数值)，数值越大优先级越高

    fun addValue(str: String) {
        value = value + str
    }

    private fun getPriority(): Int {
        if ("+-".contains(value)) return 1
        if ("*/%".contains(value)) return 2
        return 0
    }
}
